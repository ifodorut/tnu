function [statesXi, LFP] = spm_seizure_demo(dbs)
% Demo routine for local field potential models
%==========================================================================
% 
% This routine illustrates how one can model induced responses (e.g.,
% seizure onset in terms of exogenously forced changes in model parameters -
% (e.g., recurrent inhibitory connections in a canonical microcircuit
% model. This calls on extra parameters X and Y. X couples input to
% parameters, while Y couples hidden states to parameters.  Here we use
% exogenous input to change the parameters and the ensuing Jacobian to
% elicit fast gamma activity.
%
%__________________________________________________________________________
% Copyright (C) 2008 Wellcome Trust Centre for Neuroimaging
 
% Karl Friston
% $Id: spm_seizure_demo.m 7679 2019-10-24 15:54:07Z spm $ 
 
clear all;
% Model specification
%==========================================================================
rng('default')


% number of regions in coupled map lattice
%--------------------------------------------------------------------------
Nc    = 1;
Ns    = 2;
options.spatial  = 'LFP';
options.model    = 'TFM';
options.analysis = 'TFA';

% sub-population showing effect
%--------------------------------------------------------------------------
pop = [3, 4];

M.dipfit.model = options.model;
M.dipfit.type  = options.spatial;
M.dipfit.Nc    = Nc;
M.dipfit.Ns    = Ns;
M.Hz           = 1:64;

%A{1} = ones(Ns, Ns)*100;
%A{2} = ones(Ns, Ns)*100;
%A{3} = ones(Ns, Ns)*100;
%A{4} = ones(Ns, Ns);

A{1} = zeros([Ns, Ns]) + [rand rand; rand rand];
A{2} = zeros([Ns, Ns]);
A{3} = zeros([Ns, Ns]);


B{1} = zeros(Ns,Ns);
%B{2} = zeros(Ns,Ns);

% get priors
%--------------------------------------------------------------------------
pE     = spm_dcm_neural_priors(A,B,[0;0],options.model);
pE     = spm_L_priors(M.dipfit,pE);
pE     = spm_ssr_priors(pE);
x      = spm_dcm_x_neural(pE,options.model);

% eliminate channel noise and make innovations white
%--------------------------------------------------------------------------
pE.a   = [0 0; 0 0];               % log amplitude and f^(-a) exponent
pE.b   = [-8; 0];                  % log amplitude and f^(-a) exponent
pE.c   = [-8; 0];                  % log amplitude and f^(-a) exponent


% exogenous input-dependent parameters
%==========================================================================        
np     = length(spm_vec(pE)); %nr of params
nx     = length(spm_vec(x )); %nr of state variables
nu     = size(pE.C,2); %nr of inputs
i      = spm_fieldindices(pE,'G'); %the index of the parameter that will suffer changes in fx_tfm
pE.X   = sparse(i(pop),1,1,np,nu);
pE.Y   = sparse(i(pop),1,1,np,nx); %sparse(np,nx);
u      = sparse(1,nu);

u_dbs  = sparse(4,nu);

% create LFP model
%--------------------------------------------------------------------------
M.f    = 'spm_fx_tfm';
M.g    = 'spm_gx_erp';
M.h    = 'spm_fx_cmc_tfm';
M.x    = x;
M.n    = nx;
M.pE   = pE;
M.m    = nu;
M.l    = Nc;

M.U_dbs = u_dbs;
M.u_dbs = [0;0;0;0];

% Volterra Kernels and transfer functions
%==========================================================================
spm_figure('GetWin','Volterra kernels and transfer functions');

 
% augment and bi-linearise (with delays)
%--------------------------------------------------------------------------
[f,J,D]       = spm_fx_tfm(x,u,pE,M);
M.u           = sparse(Ns,1);
M.D           = D;
[M0,M1,L1,L2] = spm_bireduce(M,pE);


% compute kernels (over 64 ms)
%--------------------------------------------------------------------------
N          = 64;
dt         = 1/1000;
t          = (1:N)*dt*1000;
[K0,K1,K2] = spm_kernels(M0,M1,L1,L2,N,dt);
 
subplot(2,2,1)
plot(t,K1(:,:,1))
title('1st-order Volterra kernel','FontSize',16)
axis square
xlabel('time (ms)')
 
subplot(2,2,2)
imagesc(t,t,K2(1:64,1:64,1,1,1))
title('2nd-order Volterra kernel','FontSize',16)
axis square
xlabel('time (ms)')


% compute transfer functions for different (self) inhibitory connections
%--------------------------------------------------------------------------
p = linspace(-2,2,64);
for i = 1:length(p)
    P        = pE;
    P.G(pop) = p(i);
    [G w]    = spm_csd_mtf(P,M);
    GW(:,i)  = abs(G{1});
end

subplot(2,2,3)
plot(w,GW)
xlabel('frequency {Hz}')
title('transfer function','FontSize',16)
axis square

subplot(2,2,4)
imagesc(p,w,log(GW))
title('transfer functions','FontSize',16)
ylabel('Frequency')
xlabel('Inhibitory connection','FontSize',16)
axis xy
axis square

% Integrate system to see response (time-frequency)
%==========================================================================
spm_figure('GetWin','spontaneous fluctuations');


% remove M.u to invoke exogenous inputs
%--------------------------------------------------------------------------
try 
    M = rmfield(M,'u'); 
end

N     = 512;
U.dt  = 4/1000;
t     = (1:N)'*U.dt;
U.u   = sparse(N,M.m);
global dP_global1
global dP_global2

global G2_1
global G2_2

global variable
global xstates

xstates = zeros(16,N);
dP_global = [];
G_2 = [];
variable = [];
M.U_dbs = sparse(N,4);
M.u_dbs = [0;0;0;0];

% exogenous input
%--------------------------------------------------------------------------
% U_exo(1) = 1*tanh((t - 1)*8)/2; % normpdf(t,1,0.1)/3;
% U_exo(2) = 0*tanh((t - 1)*8)/2; % normpdf(t,1,0.1)/3;
%U_exo(U_exo < 0) = 0;

U.u(:,1) = 1*tanh((t - 1)*8)/2;


%U.u(:,2) = 0.*t;
%U.u(:,2) = 0*t;
%M.U_dbs(:,1) = tanh((t - 0.5)*8)/2 *50;

alpha = 2 * pi;
beta = 10;
M.U_dbs(:,1) = 0 * sin(alpha*(t-2)*130);    %
M.U_dbs(:,2) = 0 * sin(alpha*(t-2)*130); %tanh((t - 1)*8)/2 *50; % sin(alpha*(t.^beta));
M.U_dbs(:,3) = 0 * sin(alpha*(t-2)*130); %0 * tanh((t - 1)*8)/2 *50; % sin(alpha*(t.^beta));
M.U_dbs(:,4) = 0 * sin(alpha*(t-2)*130); %tanh((t - 1)*8)*40; %40 * sin(alpha*(t.^beta));

M.U_dbs(:,:) = M.U_dbs(:,:) .* heaviside(t-1.6);


M.W      = inv(diag(sparse(1,1,1,1,M.n) + exp(-32)));
LFP      = spm_int_sde(pE,M,U);

% plot
%--------------------------------------------------------------------------
subplot(4,1,1)
plot(t,U.u)
xlabel('time (s)')
title('Exogenous input','FontSize',16)
spm_axis tight

% DBS input
%--------------------------------------------------------------------------
subplot(4,1,2)
plot(t,M.U_dbs)
xlabel('time (s)')
title('DBS input','FontSize',16)
spm_axis tight

% LFP - random fluctuations
%--------------------------------------------------------------------------
subplot(4,1,3)
plot(t,LFP)
xlabel('time (s)')
title('LFP response','FontSize',16)
spm_axis tight
 
% time-frequency
%--------------------------------------------------------------------------
W     = 128;
TFR   = spm_wft(LFP,w*W*U.dt,W);
subplot(4,1,4)
imagesc(t,w,spm_en(abs(TFR)));
title('time-frequency response','FontSize',16)
axis  xy
xlabel('time (s)')
ylabel('Hz')
drawnow

statesXi = xstates;

% now integrate a generative model to simulate a time frequency response
%==========================================================================
% M.f       = M.h;
% M         = rmfield(M,'h');
% csd       = spm_csd_int(pE,M,U);
% 
% predicted time frequency response
%--------------------------------------------------------------------------
% subplot(5,1,5)
% imagesc(t,w,spm_en(abs(csd{1}')));
% title('Predicted response','FontSize',16)
% axis xy
% xlabel('time (s)')
% ylabel('Hz')
% 
spm_figure('GetWin','Parameter change');
subplot(3,1,1)
plot(G2_1)
hold on
plot(G2_2)
xlabel('time (s)')
title('G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,3)
plot(variable)
xlabel('time (s)')
title('G2*S2 change','FontSize',16)
spm_axis tight

spm_figure('GetWin','LFPs');
subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

subplot(3,1,2)
plot(dP_global1)
hold on
plot(dP_global2)
xlabel('time (s)')
title('dP.G(2) change','FontSize',16)
spm_axis tight

return 

 
 